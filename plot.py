import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.ticker import ScalarFormatter

if len(sys.argv) != 3:
    raise ValueError('Invalid arguments!')

path = sys.argv[1]
filename = sys.argv[2]

# load data
experiments = np.loadtxt(os.path.join(path, 'experiments.txt'))

fully_labeled = np.loadtxt(os.path.join(path, 'fully_labeled.txt'))[:, 1].reshape(-1, 1)
partially_labeled = np.loadtxt(os.path.join(path, 'partially_labeled.txt'))[:, 1].reshape(-1, 1)

da_ss = np.loadtxt(os.path.join(path, 'da_ss.txt'))[:, 1].reshape(-1, 1)
active_da_ss = np.loadtxt(os.path.join(path, 'active_da_ss.txt'))[:, 1].reshape(-1, 1)

da = np.loadtxt(os.path.join(path, 'da.txt'))[:, 1].reshape(-1, 1)
active_da = np.loadtxt(os.path.join(path, 'active_da.txt'))[:, 1].reshape(-1, 1)

iterations = int(experiments.max(axis=0)[3])
n_experiments = experiments.shape[0] / iterations
ratios = experiments[:, 2].reshape(-1, 1)
results = np.hstack((ratios, da_ss, active_da_ss, da, active_da, partially_labeled, fully_labeled))
to_stack = []
for i in np.arange(iterations):
    to_stack.append(results[n_experiments*i:n_experiments*i+n_experiments, :])
results = np.stack(to_stack)
mean = np.mean(results, axis=0)
std = np.std(results, axis=0)

# prepare and plot data
fig, ax = plt.subplots()
x_values = mean[:, 0] * 100

da_ss = mean[:, 1]
y_da_ss = da_ss[da_ss < 0.5] * 100
x_da_ss = x_values[da_ss < 0.5]
e_da_ss = std[:, 1][da_ss < 0.5] * 100
ax.errorbar(x_da_ss, y_da_ss, e_da_ss, fmt='b-', label="DA-SS")

active_da_ss = mean[:, 2]
y_active_da_ss = active_da_ss[active_da_ss < 0.5] * 100
x_active_da_ss = x_values[active_da_ss < 0.5]
e_active_da_ss = std[:, 2][active_da_ss < 0.5] * 100
ax.errorbar(x_active_da_ss, y_active_da_ss, e_active_da_ss, fmt='b--', label="Active DA-SS")

da = mean[:, 3]
y_da = da[da < 0.5] * 100
x_da = x_values[da < 0.5]
e_da = std[:, 3][da < 0.5] * 100
ax.errorbar(x_da, y_da, e_da, fmt='g-', label="DA")

active_da = mean[:, 4]
y_active_da = active_da[active_da < 0.5] * 100
x_active_da = x_values[active_da < 0.5]
e_active_da = std[:, 4][active_da < 0.5] * 100
ax.errorbar(x_active_da, y_active_da, e_active_da, fmt='g--', label="Active DA")

partially_labeled = mean[:, 5]
y_partially_labeled = partially_labeled[partially_labeled < 0.5] * 100
x_partially_labeled = x_values[partially_labeled < 0.5]
e_partially_labeled = std[:, 5][partially_labeled < 0.5] * 100
ax.errorbar(x_partially_labeled, y_partially_labeled, e_partially_labeled, fmt='r-', label="Partially Labeled")

fully_labeled = mean[:, 6]
y_fully_labeled = fully_labeled[fully_labeled < 0.5] * 100
x_fully_labeled = x_values[fully_labeled < 0.5]
e_fully_labeled = std[:, 6][fully_labeled < 0.5] * 100
ax.errorbar(x_fully_labeled, y_fully_labeled, e_fully_labeled, fmt='k:', label="Fully Labeled")

# adjust axes
ax.set_xscale('log')
ax.set_xticks(x_values)
ax.get_xaxis().set_major_formatter(ScalarFormatter())
ax.minorticks_off()

ax.set_xlabel("fraction of labeled task in %")
ax.set_ylabel("test error in %")

plt.legend()

# plt.show()
plt.savefig(filename, dpi=300, transparent=True)
