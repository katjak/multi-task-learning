import json
import os
import re
import sys

import numpy as np

sys.path.append('SIF/src')
import data_io, params, SIF_embedding

# parameters
wordfile = 'data/glove/glove.twitter.27B.25d.txt' # word vector file, can be downloaded from GloVe website
weightfile = 'SIF/auxiliary_data/enwiki_vocab_min200.txt' # each line is a word and its frequency
weightpara = 1e-3 # the parameter in the SIF weighting scheme, usually in the range [3e-5, 3e-3]
rmpc = 1 # number of principal components to remove in SIF weighting scheme
output_path = 'data/amazon-review-helpfulness/'
input_file = 'data/raw/amazon/reviews_books_filtered.json'

def prune_reviews(texts, helpfulness):
    combined = filter(lambda x: x[0], zip(texts, helpfulness))
    helpful = filter(lambda x: x[1] > 0.5, combined)
    not_helpful = filter(lambda x: x[1] < 0.5, combined)
    portion = len(helpful) - len(not_helpful)
    if portion >= 0:
        return zip(*(helpful[portion:] + not_helpful[:]))
    else:
        return zip(*(helpful[:] + not_helpful[-portion:]))


# load corpus data
print 'load data...'
reviews = dict()
with open(input_file) as infile:
    for line in infile:
        review = json.loads(line)
        if not review['asin'] in reviews:
            reviews[review['asin']] = []
        reviews[review['asin']].append(review)
print 'data loaded'

# prepare data
print 'prepare data...'
asin_to_index = dict()
running_index = 0
sentences = []
classification = []
pattern = re.compile('[^a-zA-Z ]+', re.UNICODE)
for asin in reviews:
    # remove all non-alphabetical characters and transform to lower case
    texts = map(lambda x: x['reviewText'], reviews[asin])
    alpha_only = map(lambda x: str(pattern.sub('', x)), texts)
    prepared = map(str.lower, alpha_only)

    # use equally many helpful and not helpful reviews
    pruned_texts, pruned_helpfulness = prune_reviews(prepared, map(lambda x: x['helpful'], reviews[asin]))

    # append review texts
    asin_to_index[asin] = (running_index, len(pruned_texts))
    running_index += len(pruned_texts)
    sentences += pruned_texts
    classification += map(lambda x: x > 0.5, pruned_helpfulness)
print 'data prepared'

print 'compute embeddings...'
# load word vectors
(words, We) = data_io.getWordmap(wordfile)
# load word weights
word2weight = data_io.getWordWeight(weightfile, weightpara) # word2weight['str'] is the weight for the word 'str'
weight4ind = data_io.getWeight(words, word2weight) # weight4ind[i] is the weight for the i-th word
# load sentences
x, m = data_io.sentences2idx(sentences, words) # x is the array of word indices, m is the binary mask indicating whether there is a word in that location
w = data_io.seq2weight(x, m, weight4ind) # get word weights

# set parameters
params = params.params()
params.rmpc = rmpc
# get SIF embedding
embedding = SIF_embedding.SIF_embedding(We, x, w, params) # embedding[i,:] is the embedding for sentence i
print 'embeddings computed'

# attach classification
classification = np.reshape(np.array(classification) * 1.0, (-1, 1))
embedding = np.hstack((embedding, classification))

# split into tasks
tasks = dict()
for asin in asin_to_index:
    start = asin_to_index[asin][0]
    end = start + asin_to_index[asin][1]
    tasks[asin] = embedding[start:end]

# output data
print 'store tasks...'
if not os.path.exists(output_path):
    os.makedirs(output_path)

for asin in tasks:
    np.savetxt('%s%s.txt' % (output_path, asin), tasks[asin])
print 'tasks stored'
