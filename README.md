# Multi-Task Learning with Labeled and Unlabeled Tasks (Evaluation)

## Setup instructions

Clone repository:
```
git clone --recursive https://gitlab.com/katjak/multi-task-learning.git
cd multi-task-learning
```

Setup virtual environment:
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt -r SIF/requirements.txt
```

Fix SIF:
```
cd SIF
git apply ../fix-sif-list-init.patch
cd ..
```

Download and extract corpora:
```
mkdir -p data/downloads
cd data/downloads
wget http://cvml.ist.ac.at/productreviews/MDPR.zip
wget http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Books_5.json.gz
wget http://nlp.stanford.edu/data/glove.twitter.27B.zip

cd ..
mkdir -p raw/amazon glove amazon-product-reviews/extraction

unzip downloads/MDPR.zip -d amazon-product-reviews
mv amazon-product-reviews/MDPR/products/* amazon-product-reviews/
rm -rf amazon-product-reviews/MDPR

gunzip downloads/reviews_Books_5.json.gz
mv downloads/reviews_Books_5.json raw/amazon/reviews_books.json

unzip downloads/glove.twitter.27B.zip -d glove

rm -rf downloads
cd ..
```

## Evaluation instructions

### Generate processable corpora

- Synthetic data
  ```
  python gen_bivariate_gaussian.py 'data/bivariate-gaussian' 1000 2000
  python prepare_corpus.py 'data/bivariate-gaussian' 'eval_data/bivariate-gaussian' 1000 500
  ```
- Amazon product reviews
  ```
  python prepare_corpus.py 'data/amazon-product-reviews' 'eval_data/amazon-product-reviews' 500 0
  ```
- Amazon review helpfulness
  ```
  python filter_amazon_corpus.py
  python gen_amazon_corpus.py
  python prepare_corpus.py 'data/amazon-review-helpfulness' 'eval_data/amazon-review-helpfulness' 200 0
  ```

### Run evaluation

- Synthetic data
  ```
  python eval.py 'eval_data/bivariate-gaussian' 'eval_results/bivariate-gaussian' 1000 2 10 \
  '[(1000,100,0.005), (1000,100,0.01), (1000,100,0.025), (1000,100,0.05), (1000,100,0.075), \
  (1000,100,0.1), (1000,100,0.2), (1000,100,0.3), (1000,100,0.5)]'
  ```
- Amazon product reviews
  ```
  python eval.py 'eval_data/amazon-product-reviews' 'eval_results/amazon-product-reviews' 957 25 10 \
  '[(500,400,0.005), (500,400,0.01), (500,400,0.025), (500,400,0.05), (500,400,0.075), \
  (500,400,0.1), (500,400,0.2), (500,400,0.3), (500,400,0.5)]'
  ```
- Amazon review helpfulness
  ```
  python eval.py 'eval_data/amazon-review-helpfulness' 'eval_results/amazon-review-helpfulness' 125 25 10 \
  '[(200,100,0.025), (200,100,0.05), (200,100,0.075), (200,100,0.1), (200,100,0.2), (200,100,0.3), (200,100,0.5)]'
  ```

### Parameter overview

- `eval.py`
  ```
  python eval.py <input> <results> <tasks> <dimension> <iterations> <experiments>
  ```
  - `<input>`: path to input data (4 files per task: `_train_x.txt`, `_train_y.txt`, `_test_x.txt`, `_test_y.txt`)
  - `<result>`: directory where to store results (**is cleared and removed by this script before evaluation starts**)
  - `<tasks>`: number of tasks
  - `<dimension>`: number of dimension of the input data (x only)
  - `<iterations>`: number iteration to perform
  - `<experiments>`: (Python) list of experiments to evaluate; a single experiment is characterized by a triple (# samples, # labels, ratio labeled to unlabeled tasks)
- `gen_bivariate_gaussian.py`
  ```
  python gen_bivariate_gaussian.py <output> <tasks> <samples>
  ```
  - `<output>`: directory where to store corpus
  - `<tasks>`: number of tasks to generate
  - `<samples>`: number of samples to generate per task
- `prepare_corpus.py`
  ```
  python prepare_corpus.py <input> <output> <train> <test>
  ```
  - `<input>`: directory where to find input corpus
  - `<output>`: directory where to store processed corpus
  - `<train>`: number of samples to use as training data
  - `<test>`: maximal number of samples to use as test data (if 0 use all remaining (non-training) samples)

## References

- [1] A. Pentina and C. H. Lampert. "Multi-task Learning with Labeled and Unlabeled Tasks”, ICML 2017  
  http://cvml.ist.ac.at/productreviews/
- [2] J.J. McAuley, C. Targett, J. Shi and A. van den Hengel "Image-based recommendations on styles and substitutes”, SIGIR 2015  
  http://jmcauley.ucsd.edu/data/amazon/
- [3] S. Arora, Y. Liang and T. Ma. "A simple but Tough-to-Beat Baseline for Sentence Embeddings” ICLR 2017  
  https://github.com/PrincetonML/SIF
- [4] J. Pennington, R. Socher and C. D. Manning "GloVe: Global Vectors for Word Representation”, EMNLP 2014  
  https://nlp.stanford.edu/projects/glove/