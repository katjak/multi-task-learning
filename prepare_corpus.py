import math
import os
import sys

import numpy as np

if len(sys.argv) != 5:
    raise ValueError('Invalid arguments!')

input_path = sys.argv[1]
output_path = sys.argv[2]
training_samples = int(sys.argv[3])
max_test_samples = int(sys.argv[4])

if not os.path.exists(output_path):
    os.makedirs(output_path)

input_files = os.listdir(input_path)
input_files.sort()
n_files = len(input_files)
digits = int(math.log10(n_files)) + 1
file_id = 0
for filename in input_files:
    file_id += 1
    input_data = np.loadtxt(os.path.join(input_path, filename))

    pos_idxs = np.argwhere(input_data[:, -1] != 0).flatten()
    neg_idxs = np.argwhere(input_data[:, -1] == 0).flatten()

    pos_train_idxs = np.random.choice(pos_idxs, training_samples / 2)
    neg_train_idxs = np.random.choice(neg_idxs, training_samples / 2)

    pos_test_idxs = np.setdiff1d(pos_idxs, pos_train_idxs)
    neg_test_idxs = np.setdiff1d(neg_idxs, neg_train_idxs)

    if max_test_samples:
        pos_test_idxs = pos_test_idxs[:max_test_samples / 2]
        neg_test_idxs = neg_test_idxs[:max_test_samples / 2]

    train_data = input_data[np.hstack((pos_train_idxs, neg_train_idxs))]
    test_data = input_data[np.hstack((pos_test_idxs, neg_test_idxs))]

    name_prefix = os.path.join(output_path, 'task_%s_' % str(file_id).zfill(digits))
    np.savetxt(name_prefix + 'train_x.txt', train_data[:, :-1])
    np.savetxt(name_prefix + 'train_y.txt', train_data[:, -1] * 2 - 1)
    np.savetxt(name_prefix + 'test_x.txt', test_data[:, :-1])
    np.savetxt(name_prefix + 'test_y.txt', test_data[:, -1] * 2 - 1)
