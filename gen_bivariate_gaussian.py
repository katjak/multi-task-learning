import math
import numpy as np
import os
import sys

if len(sys.argv) != 4:
    raise ValueError('Invalid arguments!')

data_path = sys.argv[1]
if not os.path.exists(data_path):
    os.makedirs(data_path)

# parameters for generator
n_tasks = int(sys.argv[2])
n_samples = int(sys.argv[3])
n_features = 2
digits = int(math.log10(n_tasks)) + 1

# parameters for distribution
mean_low = -5.
mean_high = 5.

for t in np.arange(n_tasks) + 1:
    # Gaussian distribution with mean [mean_low, mean_high)^n_features and covariance matrix I^n_features
    mean = np.random.uniform(mean_low, mean_high, n_features)
    cov = np.identity(n_features)
    samples = np.random.multivariate_normal(mean, cov, n_samples)

    # compute enclosed angle of position vectors
    angles = np.arctan2(samples[:, 1], samples[:, 0]) - np.arctan2(mean[1], mean[0])
    classes = np.reshape((angles > 0) * 1., (-1, 1))

    np.savetxt(os.path.join(data_path, 'task_%s.txt' % str(t).zfill(digits)), np.hstack((samples, classes)))
