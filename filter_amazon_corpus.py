import json

# parameters
input_file = 'data/raw/amazon/reviews_books.json'
output_file = 'data/raw/amazon/reviews_books_filtered.json'
lower_bound = 0.4
upper_bound = 0.6
cutoff = 300

# prune a single review and compute the 'helpfulness ratio'
def process_review(review):
    if not review['helpful'][1] > 0:
        return False
    ratio = float(review['helpful'][0]) / review['helpful'][1]

    try:
        del review['reviewerID']
        del review['reviewerName']
        del review['overall']
        del review['summary']
        del review['unixReviewTime']
        del review['reviewTime']
    except:
        pass

    review['helpful'] = ratio
    return True

# read file and process each review
reviews = dict()
helpful = dict()
with open(input_file) as infile:
    for line in infile:
        review = json.loads(line)

        if not process_review(review):
            continue
        if lower_bound < review['helpful'] < upper_bound:
            continue

        if review['asin'] in reviews:
            reviews[review['asin']] += 1
            helpful[review['asin']] += 1 if review['helpful'] >= upper_bound else -1
        else:
            reviews[review['asin']] = 1
            helpful[review['asin']] = 1 if review['helpful'] >= upper_bound else -1

# only use reviews that make the cutoff
keep = [asin for asin in reviews if reviews[asin] - abs(helpful[asin]) >= cutoff]
with open(input_file) as infile:
    with open(output_file, 'w') as outfile:
        for line in infile:
            review = json.loads(line)

            if not process_review(review):
                continue
            if lower_bound < review['helpful'] < upper_bound:
                continue

            if review['asin'] in keep:
                json.dump(review, outfile)
                outfile.write('\n')
