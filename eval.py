import datetime
import math
import os
import shutil
import sys
import time

import numpy as np

import main_code as mc

# parameters
data_path = ''
results_path = ''
n_tasks = 0
dim = 0
vc_dim = 0
n_iterations = 0
experiments = []


class Experiment:
    unlabeled_data = []
    data = []
    labels = []
    test_data = []
    test_labels = []

    n_samples = 0
    n_labels = 0
    labeling_ratio = 1.0

    not_run = []

    def __init__(self, n_samples, n_labels, labeling_ratio):
        self.n_samples = n_samples
        self.n_labels = n_labels
        self.labeling_ratio = labeling_ratio
        self.not_run = np.empty((n_tasks + 2))
        self.not_run[:] = np.nan

        self.unlabeled_data = []
        self.data = []
        self.labels = []
        self.test_data = []
        self.test_labels = []

        self.load_data()

    def load_data(self):
        digits = int(math.log10(n_tasks)) + 1

        # load all tasks
        for task in np.arange(n_tasks) + 1:
            name_prefix = os.path.join(data_path, 'task_%s_' % str(task).zfill(digits))

            # load training data
            full_x = np.loadtxt(name_prefix + 'train_x.txt')  # features
            full_y = np.loadtxt(name_prefix + 'train_y.txt')  # class

            # trim input according to n_samples
            offset = (len(full_x) - self.n_samples) / 2
            if offset > 0:
                full_x = full_x[offset:-offset]
                full_y = full_y[offset:-offset]

            # 1,-1 classification -> true,false classification
            mask = full_y == 1

            # list all indices and find the ones that match the respective classes
            indices_all = np.arange(self.n_samples)
            indices_pos = indices_all[mask[:]]
            indices_neg = indices_all[~mask[:]]

            # use only n_labels many training entries: 1/2 positive & 1/2 negative classification
            indices_training = np.hstack([indices_pos[:self.n_labels / 2], indices_neg[:self.n_labels / 2]])
            x = full_x[indices_training[:], :]
            y = full_y[indices_training[:]]

            # load testing/evaluation data
            test_x = np.loadtxt(name_prefix + 'test_x.txt')
            test_y = np.loadtxt(name_prefix + 'test_y.txt')

            self.unlabeled_data.append(full_x)
            self.data.append(x)
            self.labels.append(y)
            self.test_data.append(test_x)
            self.test_labels.append(test_y)

    def save_results(self, filename, results):
        with open(os.path.join(results_path, filename), 'a') as outfile:
            np.savetxt(outfile, results.reshape(1, -1))

    def train_fully_labeled(self):
        start = time.time()

        centroids, assignments = np.arange(n_tasks), np.arange(n_tasks)
        [data_centers, labels_centers] = [[self.data[task] for task in centroids], [self.labels[task] for task in centroids]]
        w, b = mc.assign_predictors_SS(data_centers, labels_centers, centroids, assignments)

        total = time.time() - start
        print 'fully labeled(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('fully_labeled.txt', np.hstack(([total, mean], error)))

    def train_partially_labeled(self, k):
        if k <= 1 or k * self.n_labels / n_tasks < 2:
            self.save_results('partially_labeled.txt', self.not_run)
            return

        start = time.time()

        centroids, assignments = np.arange(n_tasks), np.arange(n_tasks)
        [data_centers, labels_centers] = [[self.data[task] for task in centroids], [self.labels[task] for task in centroids]]
        # trim data
        available_labels = k * self.n_labels / n_tasks
        pos_labels = available_labels / 2
        neg_labels = available_labels - pos_labels
        [data_centers, labels_centers] = [[np.vstack((d[:pos_labels], d[-neg_labels:])) for d in data_centers],
                                          [np.hstack((l[:pos_labels], l[-neg_labels:])) for l in labels_centers]]
        w, b = mc.assign_predictors_SS(data_centers, labels_centers, centroids, assignments)

        total = time.time() - start
        print 'partially labeled(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('partially_labeled.txt', np.hstack(([total, mean], error)))

    def train_da_ss(self, k, discrepancies):
        if k <= 1:
            self.save_results('da_ss.txt', self.not_run)
            return

        start = time.time()

        centroids, assignments = mc.select_random_centers_SS(discrepancies, k)
        [data_centers, labels_centers] = [[self.data[task] for task in centroids], [self.labels[task] for task in centroids]]
        w, b = mc.assign_predictors_SS(data_centers, labels_centers, centroids, assignments)

        total = time.time() - start
        print 'DA-SS(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('da_ss.txt', np.hstack(([total, mean], error)))

    def train_active_da_ss(self, k, discrepancies):
        if k <= 1:
            self.save_results('active_da_ss.txt', self.not_run)
            return

        start = time.time()

        self.centroids_ss, assignments = mc.select_active_centers_SS(discrepancies, k)
        [data_centers, labels_centers] = [[self.data[task] for task in self.centroids_ss], [self.labels[task] for task in self.centroids_ss]]
        w, b = mc.assign_predictors_SS(data_centers, labels_centers, self.centroids_ss, assignments)

        total = time.time() - start
        print 'Active DA-SS(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('active_da_ss.txt', np.hstack(([total, mean], error)))

    def train_da(self, k, discrepancies):
        if k <= 1:
            self.save_results('da.txt', self.not_run)
            return

        start = time.time()

        centroids, alphas = mc.select_random_centers(discrepancies, vc_dim, k, self.n_labels)
        [data_centers, labels_centers] = [[self.data[task] for task in centroids], [self.labels[task] for task in centroids]]
        w, b = mc.assign_predictors(data_centers, labels_centers, centroids, alphas)

        total = time.time() - start
        print 'DA(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('da.txt', np.hstack(([total, mean], error)))

    def train_active_da(self, k, discrepancies):
        if k <= 1:
            self.save_results('active_da.txt', self.not_run)
            return

        start = time.time()

        centroids, alphas = mc.select_active_centers(self.centroids_ss, discrepancies, vc_dim, k, self.n_labels)
        [data_centers, labels_centers] = [[self.data[task] for task in centroids], [self.labels[task] for task in centroids]]
        w, b = mc.assign_predictors(data_centers, labels_centers, centroids, alphas)

        total = time.time() - start
        print 'Active DA(sec): ', total

        error = mc.evaluate(w, b, self.test_data, self.test_labels)
        mean = np.mean(error)
        print 'test error: ', mean

        self.save_results('active_da.txt', np.hstack(([total, mean], error)))

    def run(self):
        k = int(n_tasks * self.labeling_ratio)

        discrepancies = mc.estimate_discrepancy(self.unlabeled_data)
        # sanitize values (non-negative)
        discrepancies[discrepancies <= 0] = 1e-8
        np.fill_diagonal(discrepancies, 0.)

        with open(os.path.join(results_path, 'discrepancies.txt'), 'a') as outfile:
            outfile.write('# slice\n')
            np.savetxt(outfile, discrepancies)

        self.train_fully_labeled()
        self.train_partially_labeled(k)
        self.train_da_ss(k, discrepancies)
        self.train_active_da_ss(k, discrepancies)
        self.train_da(k, discrepancies)
        self.train_active_da(k, discrepancies)

        return self


if __name__ == '__main__':
    if len(sys.argv) != 7:
        raise ValueError('Invalid arguments!')

    data_path = sys.argv[1]
    results_path = sys.argv[2]
    n_tasks = int(sys.argv[3])
    dim = int(sys.argv[4])
    n_iterations = int(sys.argv[5])
    experiments = eval(sys.argv[6])
    vc_dim = dim + 1

    if os.path.exists(results_path):
        shutil.rmtree(results_path)
    os.makedirs(results_path)
    with open(os.path.join(results_path, 'discrepancies.txt'), 'a') as outfile:
        outfile.write('# Array shape: (%d, %d, %d)\n' % (len(experiments) * n_iterations, n_tasks, n_tasks))

    for i in np.arange(n_iterations) + 1:
        for s, l, r in experiments:
            print ''
            print datetime.datetime.now()
            print '# samples: ', s
            print '# labels: ', l
            print 'labeled tasks ratio: ', r
            print 'iteration: ', i

            start_global = time.time()

            e = Experiment(s, l, r)
            e.run()

            total_global = time.time() - start_global

            print 'time(sec): ', total_global

            with open(os.path.join(results_path, 'experiments.txt'), 'a') as outfile:
                np.savetxt(outfile, np.array([s, l, r, i, total_global]).reshape(1, -1))
