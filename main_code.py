import numpy

# List of main functions:

# estimate_discrepancy(data, bias = True) - estimation of discrepancy based on squared loss (without regularization)
# evaluate(w, b, data, labels) - returns a vector of test errors
# select_active_centers_SS(D, k, maxit=100, nrestarts=10) - selects labeled tasks and assignments for Active DA-SS
# select_random_centers_SS(D, k) - selects random tasks to have labeled and aassignments based on discrepancy for DA-SS
# assign_predictors_SS(data, labels, centroids, assignments) - assigns predictors for all tasks for (Active) DA-SS
# select_active_centers(centroids0, D, VCdim, k, m, maxit=100, nrestarts=10) - selects labeled tasks and computes alphas
#                                                                              for Active DA
# select_random_centers(D, VCdim, k, m, maxit=100) - selects random tasks to have labeled and computes alphas for DA
# assign_predictors(data, labels, centroids, alphas) - assigns predictors to all tasks based on the data for labeled
#                                                          tasks data for labeled tasks and alphas for (Active) DA

# Parameters:

# data - list of T data matrices for T tasks, each matrix is nxd with n dapa points of dimensionality d in rows
# labels - list of label vectors for T tasks
# w - list of weight vectors
# b - list of bias terms
# D - a TxT symmetric matrix of pairwise discrepancies between T tasks
# k - number of labeled tasks
# centroids - indicies of labeled tasks
# assignments - a TxT matrix, what is denoted (\alpha^t_i) in the paper, t - index of the row, i - index of the column
# VCdim - VC dimension of the hypothesis set, i.e. dimensionality of the data + 1
# m - number of labeled samples per labeled task


pos_C = numpy.concatenate([numpy.array([0]), 10.**numpy.arange(-17,10)]) #values of the regularization parameter
                                                                            # for cross-validation


def estimate_discrepancy(data, bias=True):
    n = len(data)
    D = numpy.zeros((n, n))
    for task1 in numpy.arange(n):  # doesn't need labels or test data
        X1 = data[task1]
        m1 = numpy.shape(X1)[0]
        for task2 in numpy.arange(task1 + 1, n):
            X2 = data[task2]
            m2 = numpy.shape(X2)[0]
            Y = numpy.hstack([numpy.ones(m1), -1. * numpy.ones(m2)])
            X = numpy.vstack([X1, X2])
            [w, b] = trainRegression(X, Y, numpy.ones(m1 + m2) * 1. / (m1 + m2), 0., bias)
            pred = numpy.dot(w, X.T) + b
            score = numpy.mean(pred * Y <= 0.)
            D[task1, task2] = 1. - 2 * score

    D = D + D.T
    return D


def fully_labeled(data, labels):
    ntasks = len(data)
    w_all = []
    b_all = []
    C_all = []
    for task in numpy.arange(ntasks):
        m = numpy.shape(data[task])[0]
        alphas = numpy.ones(m) * 1. / m
        [w, b, C] = doCVRegression(data[task], labels[task], alphas)
        w_all.append(w)
        b_all.append(b)
        C_all.append(C)
    return w_all, b_all, C_all


def evaluate(w, b, test_data, test_labels):
    ntasks = len(w)
    error = numpy.zeros(ntasks)
    for task in numpy.arange(ntasks):
        pred = numpy.dot(test_data[task], w[task])+b[task]
        error[task] = numpy.mean(pred*test_labels[task] <= 0)
    return error


def select_active_centers_SS(D, k, maxit=100, nrestarts=10):
    best_score = numpy.inf
    for it in range(nrestarts):
        score, centroids, assignments = do_kmediodEM(D, k, maxit)
        if score < best_score:
            best_score = score
            best_centroids = centroids
            best_assignments = assignments
    return best_centroids, best_assignments


def select_random_centers_SS(D, k):
    centroids = numpy.random.choice(D.shape[0], size=k, replace=False)
    assignments = centroids[D[:, centroids].argmin(axis=1)]
    return centroids, assignments


def assign_predictors_SS(data_centers, labels_centers, centroids, assignments):
    ntasks = numpy.size(assignments)
    [w_ind, b_ind, C] = fully_labeled(data_centers, labels_centers)
    w = []
    b = []
    for task in numpy.arange(ntasks):
        w.append(w_ind[numpy.where(centroids == assignments[task])[0][0]])
        b.append(b_ind[numpy.where(centroids == assignments[task])[0][0]])
    return w, b


def select_active_centers(centroids0, D, VCdim, k, m, maxit=100, nrestarts=10):
    best_score, best_alphas = do_weighted_kmediod(centroids0, D, VCdim, k, m)
    T = numpy.shape(D)[0]
    alltasks = numpy.arange(T)
    for it in range(nrestarts - 1):
        centroids = kmediod_initialization(D, k)
        score, alphas = do_weighted_kmediod(centroids, D, VCdim, k, m)
        if score < best_score:
            best_score = score
            best_alphas = alphas
    mask = numpy.sum(best_alphas, axis=0) > 0
    centroids = alltasks[mask[:]]
    return centroids, best_alphas


def select_random_centers(D, VCdim, k, m, maxit=100):
    T = numpy.shape(D)[0]
    centroids = numpy.random.choice(T, size=k, replace=False)
    alphas = minfun(centroids, D, VCdim, m, maxit=maxit)

    return centroids, alphas


def assign_predictors(data_centers, labels_centers, centroids, alphas):
    ntasks = numpy.shape(alphas)[0]
    w = []
    b = []
    data_merged = numpy.concatenate(data_centers, axis = 0)
    labels_merged = numpy.concatenate(labels_centers)
    for task in numpy.arange(ntasks):
        weights = []
        for i  in range(numpy.size(centroids)):
            m = numpy.shape(data_centers[i])[0]
            weights = weights + (numpy.ones(m)*alphas[task, centroids[i]]*1./m).tolist()
        weights = numpy.array(weights)
        [w0, b0, C_wkm] = doCVRegression(data_merged, labels_merged, weights)
        w.append(w0)
        b.append(b0)
    return w, b

###############################################################
###############################################################
############ Supplementary functions ##########################
###############################################################
###############################################################


def trainRegression(X, Y, alphas, C, bias = True):
    
    n,d = X.shape
    if bias:
        X_exp = numpy.hstack([X, numpy.ones((n,1))])
    else:
        X_exp = X
    X_weighted = numpy.multiply(X_exp, numpy.sqrt(alphas).reshape((n,1)))
    Y_weighted = numpy.multiply(Y, numpy.sqrt(alphas))
    K = numpy.dot(X_weighted.T, X_weighted)
    rhs = numpy.dot( X_weighted.T, Y_weighted)
    if bias:
        E = numpy.eye(d+1)
        E[-1,-1]=0.
    else:
        E = numpy.eye(d)
    w_exp = numpy.dot(numpy.linalg.pinv(K+C*E),rhs)
    if bias:
        w = w_exp[:-1]
        b = w_exp[-1]
    else:
        w = w_exp
        b = 0.
    
    return [w,b]


def doCVRegression(X, Y, alphas):
# do 5 times 5-fold CV with possible values for C = pos_C

    n,d = X.shape
    
    cv_acc = numpy.zeros(len(pos_C))
    l = len(pos_C)
    
    allind = numpy.arange(n)   # indicies of all samples
    mask = Y > 0               # indicies of samples with positive label 
    
    for cv_it in range(5):
        
        ind_pos = numpy.random.permutation(allind[mask[:]])
        ind_neg = numpy.random.permutation(allind[~mask[:]])
        ind = numpy.hstack((ind_pos, ind_neg))                #permuted indicies
        
        for split in range(5):
            
            test_mask = numpy.zeros(ind.shape, dtype = bool)
            test_mask[split::5] = 1                           #mask of test samples wrt permuted indicies
            
            test_ind = ind[test_mask[:]]                      #indicies of test samples
            train_ind = ind[~test_mask[:]]                    #indicies of train samples
            
            test_data = X[test_ind[:],:]
            test_labels = Y[test_ind[:]]
            test_alphas = alphas[test_ind[:]]
            train_data = X[train_ind[:],:]
            train_labels = Y[train_ind[:]]
            train_alphas = alphas[train_ind[:]]/numpy.sum(alphas[train_ind[:]])
            
            for it in range(l):
                C = pos_C[it]
                [w,b] = trainRegression(train_data, train_labels, train_alphas, C)
                predictions = numpy.dot(test_data, w)+b
                cv_acc[it] = cv_acc[it] + numpy.dot(predictions*test_labels > 0, test_alphas)
                
    opt = numpy.where(cv_acc == numpy.max(cv_acc))
    if numpy.size(opt[0]) == 1:
        opt_C = pos_C[opt[0][0]] 
    else:
        if opt[0][0] == 1:
            opt_C = pos_C[opt[0][1]]
        else:
            opt_C = pos_C[opt[0][0]]
            
    [w, b] = trainRegression(X, Y, alphas, opt_C)
        
    return  [w, b, opt_C]


def kmediod_initialization(D,k):
# initialization heuristic from k-means++ method

    centroids = []
    centroids.append(numpy.random.choice(D.shape[0], size = 1))
    for i in range(1,k):
        dist = numpy.array([min([D[x,c] for c in centroids]) for x in numpy.arange(D.shape[0])])
        probs = dist/dist.sum()
        cumprobs = probs.cumsum()
        r = numpy.random.rand()
        for j,p in enumerate(cumprobs):
            if r < p:
                i = j
                break
        centroids.append(i)
    
    return numpy.array(centroids, dtype = int)


def do_kmediodEM(D, k, maxit=1000):
    """Find k-mediods using local search, as in 
    [Hae-Sang Park , Chi-Hyuck Jun, "A simple and fast algorithm for 
    K-medoids clustering", Expert Systems with Applications, Volume 36, 
    Issue 2, Part 2, March 2009, Pages 3336-3341"""
   
    # centroids = numpy.random.choice(D.shape[0],size=k,replace=False)
    centroids = kmediod_initialization(D,k)
    
    last_score = numpy.inf
    for it in xrange(maxit):
        assignments = centroids[D[:,centroids].argmin(axis=1) ] # find nearest mediod
        new_centroids = []
        for c in centroids:
            idx = (assignments==c).nonzero()[0] # 1D array
            if numpy.size(idx > 0):
                i = D[idx][:,idx].sum(axis=1).argmin()
                new_centroids.append( idx[i] )
        centroids = numpy.asarray(new_centroids)
        score = D[range(len(assignments)),assignments].sum()
        if score == last_score:
            break
        else:
            last_score = score
    
    return score,centroids,assignments


def fun(alphas, centroids, D, VCdim, m):
# evaluate the objective from Theorem 2, skip log-factors
# T tasks, m labeled examples per centroid
# alphas = TxT matrix
# centroids k-array of centroids' indicies
# VCdim - VC dimension of the hypothesis set
    
    T = numpy.shape(D)[0]
    k = numpy.size(centroids)
    
    A = numpy.sqrt(2*VCdim/m)
    B = numpy.sqrt(0.5/m)
    
    f = numpy.sum(numpy.multiply(alphas, D))
    
    norm21 = numpy.sum(numpy.sqrt(numpy.sum(numpy.multiply(alphas, alphas), axis = 1)))
    norm12 = numpy.linalg.norm(numpy.sum(alphas, axis = 0))
    
    f = f + A*norm21 + B*norm12

    return f


def gradfun(alphas, centroids, D, VCdim, m):

    T = numpy.shape(D)[0]
    k = numpy.size(centroids)
    
    A = numpy.sqrt(2*VCdim/m)
    B = numpy.sqrt(0.5/m)
    
    grad = D
    
    div1 = numpy.sqrt(numpy.sum(numpy.multiply(alphas, alphas), axis = 1)).reshape((T,1))
    div1 = numpy.tile(div1, (1,T))
    
    div2 = numpy.tile(numpy.linalg.norm(numpy.sum(alphas, axis = 0)), (T,T))
    
    mult2 = numpy.sum(alphas, axis = 0).reshape((1,T))
    mult2 = numpy.tile(mult2, (T,1))
    
    grad = grad + A*numpy.divide(alphas, div1) + B*numpy.divide(mult2, div2)

    return grad


def project_to_simplex(y):
    """Solve min_x ||x-y||^2 sbt. x>=0 and sum(x)=1 
    using projection method from 
    http://ttic.uchicago.edu/~wwang5/papers/SimplexProj.pdf
    """
    d = len(y)
    u = numpy.sort(y)[::-1] # sort in decreasing order 
    
    v = (1-numpy.cumsum(u))/numpy.arange(1.,d+1)
    rho = numpy.where(u+v > 0)[0][-1]
    lam = v[rho]
    return numpy.clip(y+lam,0,numpy.inf)


def minfun(centroids, D, VCdim, m, prec = 0.0001, maxit=100):
# minimize f with fixed indices of centroids

    T = numpy.shape(D)[0]
    k = numpy.size(centroids)
    noncentroids = numpy.setdiff1d(numpy.arange(T), centroids)

    # initialize the alphas
    alphas = numpy.zeros((T,T))
    alphas[:,centroids[:]] = 1./k
    
    fun_new = fun(alphas, centroids, D, VCdim, m)
    fun_old = 2*fun_new+1
    it = 0
    stepsize = 0.01
    
    while fun_old - fun_new > prec and it < maxit:
        
        grad = gradfun(alphas, centroids, D, VCdim, m)            # compute gradient
        grad[:, noncentroids[:]] = 0                              # ignore all components not corresponding to centroids
        alphas -= stepsize*grad                                   # take a step
        for i in range(T):
            alphas[i,centroids[:]] = project_to_simplex(alphas[i,centroids[:]])    # poject to simplex
        it += 1                                                 # evaluate the function and update iterator
        fun_old = fun_new
        fun_new = fun(alphas, centroids, D, VCdim, m)
        
    return alphas


def do_weighted_kmediod(centroids0, D, VCdim, k, m, prec = 0.0001, maxit=100):
# method from http://www.jmlr.org/papers/volume14/bahmani13a/bahmani13a.pdf
# alphas - TxT matrix with alphas[i,j] = weight of centroid j for task i
# sum_j alphas[i,j]=1 and only k columns are non-zero

    T = numpy.shape(D)[0]

    # initialise
    centroids = centroids0
    alphas = minfun(centroids, D, VCdim, m, prec=prec, maxit=maxit)
    
    fun_new  = fun(alphas, centroids, D, VCdim, m)
    fun_old = fun_new*2+1
    it = 0
    
    best_alphas = alphas
    best_score = fun_new

    while numpy.abs(fun_old - fun_new) > prec and it < maxit:
        
        grad = gradfun(alphas, centroids, D, VCdim, m)                         # compute local gradient

        norm = numpy.linalg.norm(grad, axis = 0)                               # identify directions
        Z = numpy.argsort(-norm)[:2*k]
        Z = numpy.array(list(set(Z)-set(centroids)))                           # proposed centroids
        
        # merge supports
        centroids = numpy.hstack([centroids, Z])
        centroids = centroids.astype(int)

        # minimise over the support
        alphas = minfun(centroids, D, VCdim, m, prec=prec, maxit=maxit)
        
        # prune the estimate
        norm = numpy.linalg.norm(alphas, axis = 0)
        centroids = numpy.argsort(-norm)[:k]
        noncentroids = numpy.setdiff1d(numpy.arange(T), centroids)
        alphas[:, noncentroids[:]] = 0.
        for i in range(T):
            alphas[i,centroids[:]] = project_to_simplex(alphas[i,centroids[:]])

        # evaluate the function and update T
        it += 1
        fun_old = fun_new
        fun_new = fun(alphas, centroids, D, VCdim, m)
        
        if fun_new < best_score:
            best_alphas = alphas
            best_score = fun_new

    return best_score, best_alphas
    

    



